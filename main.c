/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private variables ---------------------------------------------------------*/

#define MainVectorTableAddr						0x08004000
#define EEPROM_PAGE_SIZE						4096
#define TIMEOT_S								8
#define TIMEOUT									(TIMEOT_S * 1000) 

static volatile uint32_t timeout_bootloader = TIMEOUT;
static volatile uint32_t timeout_CAN = 0;
static FlagStatus write_flash_flag = RESET;
static FlagStatus read_flash_flag = RESET;
static FlagStatus send_flash_flag = RESET;
static FlagStatus end_program = RESET;
static FlagStatus end_read = RESET;
static uint8_t buff[28];
static uint8_t read_buff[28];

static uint32_t message_counter = 0;
static uint32_t addr_shift = 0;
static uint8_t buff_shift = 0;
static uint8_t buff_read_shift = 0;
static uint32_t read_addr_shift = 0;
static uint32_t firmware_size = 0;



uint32_t (*pmain)(void);

void JumpMainProgr(void)
{
	unsigned int r0, r1;
	
	CAN_DeInit(MDR_CAN1);
	SYSTICK_DISABLE();
	SYSTICK_IT_DISABLE();
	
	NVIC->ICER[0]=0xFFFFFFFF;		
	SCB->VTOR = MainVectorTableAddr;
	__DSB();
	
	r0 = *(uint32_t*)(MainVectorTableAddr);
	__set_MSP(r0);
	r1 =  *(uint32_t*)(MainVectorTableAddr + 4);
	pmain = (uint32_t (*)(void)) r1;
	pmain();
}


#ifdef __CC_ARM
int main(void)
#else
void main(void)
#endif
{
	uint32_t frame;
	uint32_t i;
	uint32_t irqs;
	uint32_t Data[2] = {0 ,0};
	for (i=0;i<0x5FFFF;i++); //power stabilization
	
	Clock_Init();
	
	
	CAN1_Init();
	NVIC_init();
	
	while (1){

		if (write_flash_flag == SET){
			SYSTICK_DISABLE();
			for (i = 0; i < 24 ; i+=4){
				uint32_t attemps;
				
				frame = (buff[i + 3] << 24) + (buff[i + 2] << 16) + (buff[i + 1] << 8) + buff[i];
				irqs = NVIC->ISER[0];
				NVIC->ICER[0]=0xFFFFFFFF;
				
				
				__DSB();
				if (!(addr_shift % EEPROM_PAGE_SIZE)){
							
							EEPROM_ErasePage((MainVectorTableAddr+addr_shift), 0);
				}
				
				for (attemps = 10 ; attemps > 0 ;  attemps--){
					
					EEPROM_ProgramWord (MainVectorTableAddr+addr_shift, EEPROM_Main_Bank_Select, frame);
				
					ProgramDelay(GET_US_LOOPS(10));
					
					if (EEPROM_ReadWord(MainVectorTableAddr+addr_shift, EEPROM_Main_Bank_Select) == frame)
						break;
				}
				
				if (attemps == 0)
					ErrorWrite();
				
				NVIC->ISER[0] = irqs;
				addr_shift += 4;
			}
			SYSTICK_ENABLE();
			
			SendResponseCAN();
			
			write_flash_flag = RESET;
			
			if (end_program == SET){
				JumpMainProgr();
			}
			
			timeout_bootloader = TIMEOUT;
			CAN_ITConfig(MDR_CAN1, CAN_IT_GLBINTEN, ENABLE);
		}
		else if (read_flash_flag == SET){
			
			SYSTICK_DISABLE();
			irqs = NVIC->ISER[0];
			NVIC->ICER[0]=0xFFFFFFFF;
			
			__DSB();
				
			for(i = 0; i < 28 ; i+=4 ){
				*((uint32_t *)&read_buff[i]) = EEPROM_ReadWord(MainVectorTableAddr + read_addr_shift + i, EEPROM_Main_Bank_Select);
			}

			NVIC->ISER[0]=irqs;
			SYSTICK_ENABLE();
			
			send_flash_flag = SET;

			timeout_bootloader = TIMEOUT;
			read_flash_flag = RESET;
		}
		else if (send_flash_flag == SET){

			if(read_addr_shift == 0)
				Data[0] = 0x1A;
			else if (read_addr_shift > 0 && (read_addr_shift + 7) < firmware_size)
				Data[0] = 0x2A;
			else {
				Data[0] = 0x3A;
				end_read = SET;
			}
			read_addr_shift += 7;
			
			Data[0] += (read_buff[buff_read_shift] << 8) + (read_buff[buff_read_shift + 1] << 16) + (read_buff[buff_read_shift + 2] << 24);
			buff_read_shift += 3;
			Data[1] = read_buff[buff_read_shift] + (read_buff[buff_read_shift + 1] << 8) + (read_buff[buff_read_shift + 2] << 16) + (read_buff[buff_read_shift + 3] << 24);
			buff_read_shift += 4;
			
			if (buff_read_shift == 28)
				buff_read_shift = 0;
			
			timeout_bootloader = TIMEOUT;
			send_flash_flag = RESET;
			
			CanSend(ID_PROGRAM_DATA_RESP, Data, 8);
			
			CAN_ITConfig(MDR_CAN1, CAN_IT_GLBINTEN, ENABLE);
		}
		else if(timeout_bootloader == 0){
				JumpMainProgr();
		}
	}
}


void ErrorWrite(void)
{
	while (1)
	{
	}
}

void SysTick_Handler(void)
{
	if (timeout_bootloader) timeout_bootloader--;
	if (timeout_CAN) timeout_CAN--;
}


static void SetTimeoutCAN(void)
{
	timeout_CAN = 10;
}



void CAN1_IRQHandler(void)
{
	CAN_RxMsgTypeDef RxMessage;
	uint32_t rx_buf=0;
	uint32_t rx_count=RX_CNT_CAN;
	uint32_t Data;
		
	while (((CAN_GetBufferStatus(MDR_CAN1, rx_buf) & CAN_BUF_CON_RX_FULL)  != CAN_BUF_CON_RX_FULL ) && (rx_buf<rx_count )) rx_buf++;
		
	CAN_GetRawReceivedData(MDR_CAN1, rx_buf, &RxMessage);

	switch (CAN_EXTID_TO_STDID(RxMessage.Rx_Header.ID))
	{
		case ID_PROGRAM_DATA:
			if ((RxMessage.Data[0] & 0x0F) == 2){
				message_counter = (RxMessage.Data[0] >> 8) & 0xFF;
				Data=RxMessage.Data[0] >> 16;
			
				memcpy(&buff[buff_shift],&Data,2);
				buff_shift+=2;
				memcpy(&buff[buff_shift],&(RxMessage.Data[1]),4);
				buff_shift+=4;
			
				if ((RxMessage.Data[0] & 0x30)==0x30){
					
					end_program = SET;
					write_flash_flag = SET;
					
					SendResponseCAN();
					
					CAN_ITConfig(MDR_CAN1, CAN_IT_GLBINTEN, DISABLE);
				}
				else if (buff_shift >= 24){
					write_flash_flag = SET;
					CAN_ITConfig(MDR_CAN1, CAN_IT_GLBINTEN, DISABLE);
					buff_shift = 0;
				}
				else {
					SendResponseCAN();
				}
			}
			else if ((RxMessage.Data[0] & 0xFF) == 0x0B){
				read_flash_flag = SET;
				firmware_size = (RxMessage.Data[0] >> 8);
				CAN_ITConfig(MDR_CAN1, CAN_IT_GLBINTEN, DISABLE);
			}
			else if ((RxMessage.Data[0] & 0xFF) == 0x1B){
				if (end_read == RESET){
					if(buff_read_shift == 0)
						read_flash_flag = SET;
					else
						send_flash_flag = SET;
						CAN_ITConfig(MDR_CAN1, CAN_IT_GLBINTEN, DISABLE);
			}
		}
			break;		
	}

	CAN_ITClearRxTxPendingBit(MDR_CAN1, rx_buf, CAN_STATUS_RX_READY);
}


void SendResponseCAN(void)
{
	uint32_t Data[2];
	
	Data[0] = 0x13 + (message_counter << 8);
	Data[1] = 0;
	
	CanSend(ID_PROGRAM_DATA_RESP, Data, 8);
}


void CanSend(uint32_t ID, uint32_t *Data, uint8_t DLC)
{
	CAN_TxMsgTypeDef IN_Mess;
	uint32_t numb=RX_CNT_CAN+1;
	
	IN_Mess.Data[0]=Data[0];
	if(DLC>3)	
		IN_Mess.Data[1]=Data[1];
	else 
		IN_Mess.Data[1]=0;
	
	IN_Mess.DLC=8;
	IN_Mess.ID=CAN_STDID_TO_EXTID(ID);
	IN_Mess.IDE=CAN_ID_STD;
	IN_Mess.PRIOR_0=DISABLE;
	
	
	SetTimeoutCAN();
	
	while ((CAN_GetBufferStatus(MDR_CAN1, numb) & CAN_BUF_CON_TX_REQ) == CAN_BUF_CON_TX_REQ ){
		if (numb==MAX_BUFF_NUMB){
			numb=RX_CNT_CAN+1;
			while ((CAN_GetBufferStatus(MDR_CAN1, numb) & CAN_BUF_CON_TX_REQ) == CAN_BUF_CON_TX_REQ){
				if (timeout_CAN)
					return;
			}
			break;
		}
		else numb++;
	}
	
	
	CAN_Transmit(MDR_CAN1, numb, &IN_Mess);
	
}




#if (USE_ASSERT_INFO == 1)
void assert_failed(uint32_t file_id, uint32_t line)
{

  while (1)
  {
  }
}
#elif (USE_ASSERT_INFO == 2)
void assert_failed(uint32_t file_id, uint32_t line, const uint8_t* expr);
{

  while (1)
  {
  }
}
#endif 
